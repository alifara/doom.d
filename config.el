;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
;; (require 'privateinfo.config "~/.doom.d/privateinfo.config.el")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:

(fringe-mode '(2 . 0))
;; (setq doom-font (font-spec :family "Iosevka Nerd Font Extra Bold" :size 17))
                                        ;(setq doom-font (font-spec :family "IosevkaTerm Nerd Font Mono Semi Bold" :size 16))
;; (setq doom-font (font-spec :family "Zira Code" :size 17))
;;(setq doom-font (font-spec :family "Iosevka Nerd Font Extra Bold" :size 16))
;;
;;
;; (setq doom-font (font-spec :family "Jetbrains Mono" :size 15))
;; (setq doom-font (font-spec :family "Iosevka NF ExtraBold" :size 16))
;; (setq doom-unicode-font (font-spec :name "Noto Sans Arabic UI" :size 16))
;; (setq doom-font (font-spec :family "Jetbrains Mono" :size 24))
;; (setq doom-font (font-spec :family "Iosevka NF ExtraBold" :size 26))
;; (setq doom-unicode-font (font-spec :name "Noto Sans Arabic UI" :size 24))

(setq doom-font (font-spec :family "Iosevka NF ExtraBold" :size 16))
(setq doom-symbol-font (font-spec :name "Noto Sans" :size 12))


;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
;; (setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-opera)
;; (setq doom-theme 'kaolin-valley-dark)
;; (setq doom-theme 'doom-monokai-spectrum)
;; (setq doom-theme 'doom-monokai-pro)
;; (setq doom-theme 'doom-meltbus)
;; (setq doom-theme 'doom-dark+)
;; (setq doom-theme 'doom-dracula)
;; (setq doom-theme 'doom-one-light)
;; (setq doom-theme 'doom-badger)
(setq doom-theme 'kaolin-galaxy)

;; If you intend to use org, it is recommended you change this!
(setq org-directory "~/org/")

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
;; (setq display-line-numbers-type t)
;; (setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;;; lock cursor in place when scrolling
;; (setq scroll-preserve-screen-position 1)

;;;; from https://dotdoom.rgoswami.me/config.html

;;; helm
;; ;; Undo the helm text enlargement in childframes
;; (setq +helm-posframe-text-scale 0)

;; More sane helm
;; (after! helm
;;  ;; I want backspace to go up a level, like ivy
;;  (add-hook! 'helm-find-files-after-init-hook
;;    (map! :map helm-find-files-map
;;          "<DEL>" #'helm-find-files-up-one-level)))
;; /helm

;; always have line numbers
;; (global-display-line-numbers-mode)

;;; Tramp
(setq tramp-terminal-type "tramp")
(setq tramp-default-remote-shell "/bin/bash")

;;; company
;; C-/ to auto-complete
;; (eval-after-load "undo-tree"  ;; after! doesn't work for some reason.
;;   (define-key undo-tree-map (kbd "C-") nil))
(with-eval-after-load "company" ;; without undo-tree
  ;; (global-unset-key (kbd "C-/"))
  ;; (global-set-key (kbd "C-/") 'company-complete))
  (global-set-key (kbd "C-<space>") 'company-complete))

;; Make company ignore case when completing
(after! 'company-mode
  (setq completion-ignore-case 't)
  (setq company-dabbrev-downcase 't))
;; /compnay

;; replace stuff
(map! :leader
      (:prefix ("r" . "Replace")
       :desc "String" "s" 'replace-string
       :desc "Query" "q" 'query-replace
       :desc "At Cursor" "j" 'anzu-query-replace-at-cursor
       (:prefix ("r" . "Regexp")
        :desc "String" "s" 'replace-regexp
        :desc "Query" "q" 'query-replace-regexp)))

;; ace (window stuff)
(map! :leader  ;; Better keybinding for ace-window
      :desc "Select window using ace-window"
      :n "w a" #'ace-select-window)

;; avy, line stuff
(map! :nv "g s l" 'avy-goto-line
      :desc "Goto line using avy")
(map! :nv "g s m" 'avy-move-line
      :desc "Move line using avy")
(map! :nv "g s c" 'avy-copy-line
      :desc "Copy line using avy")

;; Hideshow, for all
(map! :leader "[" 'hs-toggle-hiding
      :desc "hs-toggle-hiding")
(map! :leader "c {" 'hs-hide-all
      :desc "hs-hide-all")
(map! :leader "c }" 'hs-show-all
      :desc "hs-show-all")

;; treemacs
(map! :leader  ;; Remap treemacs toggle
      :desc "Open treemacs pane"
      :n "o n" #'+treemacs/toggle)
(map! :leader  ;; Remap teemacs finding stuff
      :desc "Treemacs find file"
      :n "o N" 'treemacs-find-file)

;; transpose chars
(map! :leader
      :desc "Transpose chars"
      :n "t t" 'transpose-chars)

;; magit
(map! :leader  ;; magit push
      :desc "magit-push"
      :n "g p" 'magit-push)
(map! :leader  ;; magit ammend commit
      :desc "magit-commit-ammend"
      :n "g c a" 'magit-commit-amend)

;; olivetti
(map! :leader ;; olivetti-mode for foucusing
      :desc "olivetti-mode for focus"
      :n "b f" 'olivetti-mode)

;;; hydra
;; sane paste
(defhydra hydra-paste (:color red
                       :hint nil)
  "\n[%s(length kill-ring-yank-pointer)/%s(length kill-ring)] \
 [_C-j_/_C-k_] cycles through yanked text, [_p_/_P_] pastes the same text \
 above or below. Anything else exits."
  ("C-j" evil-paste-pop)
  ("C-k" evil-paste-pop-next)
  ("p" evil-paste-after)
  ("P" evil-paste-before))

(map! :n "p" #'hydra-paste/evil-paste-after
      :n "P" #'hydra-paste/evil-paste-before)

;;; disable docs in mini-buffer
(after! lsp-ui
  :config
  (setq lsp-eldoc-enable-hover 't)
  (setq lsp-ui-doc-show-with-mouse nil)
  (setq lsp-ui-doc-include-signature 't)
  (setq lsp-ui-doc-show-with-cursor nil)
  (setq lsp-ui-doc-delay 0.2)
  (setq lsp-ui-doc-position 'at-point)
  (setq eldoc-echo-area-use-multiline-p nil)
  )

(after! lsp-mode
  (set-lsp-priority! 'clangd 0)
  (set-lsp-priority! 'ccls 1))

;;; emacs and jupyter (I hope it doesn't blow up in my face)
(after! evil-org
  (setq org-babel-default-header-args:jupyter-python '((:async . "yes")
                                                       (:pandoc t)
                                                       (:kernel . "python3")))
  (setq org-babel-default-header-args:jupyter-R '((:pandoc t)
                                                  (:kernel . "ir"))))
;; (:when (featurep! :lang +jupyter)
(map! :after evil-org
      :map evil-org-mode-map
      :leader
      :desc "tangle" :n "ct" #'org-babel-tangle
      :localleader
      :desc "Hydra" :n "," #'jupyter-org-hydra/body
      :desc "Inspect at point" :n "?" #'jupyter-inspect-at-point
      :desc "Execute and step" :n "RET" #'jupyter-org-execute-and-next-block
      :desc "Delete code block" :n "x" #'jupyter-org-kill-block-and-results
      :desc "New code block above" :n "+" #'jupyter-org-insert-src-block
      :desc "New code block below" :n "=" (λ! () (interactive) (jupyter-org-insert-src-block t nil))
      :desc "Merge code blocks" :n "m" #'jupyter-org-merge-blocks
      :desc "Split code block" :n "-" #'jupyter-org-split-src-block
      :desc "Fold results" :n "z" #'org-babel-hide-result-toggle

      :map org-src-mode-map
      :localleader
      :desc "Exit edit" :n "'" #'org-edit-src-exit)

(map! :after python
      :map python-mode-map
      :localleader
      (:desc "eval" :prefix "e"
       :desc "line or region" :n "e" #'jupyter-eval-line-or-region
       :desc "defun" :n "d" #'jupyter-eval-defun
       :desc "buffer" :n "b" #'jupyter-eval-buffer))


(set-popup-rule! "*jupyter-pager*" :side 'right :size .40 :select t :vslot 2 :ttl 3)
(set-popup-rule! "^\\*Org Src*" :side 'right :size .60 :select t :vslot 2 :ttl 3 :quit nil)
(set-popup-rule! "*jupyter-repl*" :side 'bottom :size .30 :vslot 2 :ttl 3)

(after! jupyter
  (set-eval-handler! 'jupyter-repl-interaction-mode #'jupyter-eval-line-or-region))
;;/jupyter

;;; UI
;; doom-dashboard
(setq fancy-splash-image (concat doom-private-dir "splash-yay-chopped.png"))

                                        ;(require 'xah-fly-keys)
                                        ;(xah-fly-keys-set-layout "qwerty") ;; specify a layout
                                        ;(xah-fly-keys 1)
                                        ;(setq doom-leader-key "<f1>")
                                        ;(global-unset-key (kbd "C-n"))
                                        ;(global-unset-key (kbd "<escape>"))
                                        ;(global-set-key [escape] xah-fly-command-mode-activate-hook)

;;;;; I may repent some day.
;;;; Actually, evil is better.
;;; God-mode configs
;; (require 'god-mode)
;; (god-mode-all)
;; (after! 'god-mode-all
;;   (progn
;;     (global-set-key (kbd "<escape>") #'god-mode)
;;     (define-key god-local-mode-map (kbd ".") #'repeat)
;;     (define-key god-local-mode-map (kbd "i") #'god-mode-all)
;;     (setq god-mode-enable-function-key-translation nil)
;;     (setq god-exempt-major-modes nil)
;;     (setq god-exempt-predicates nil)
;;     (defun my-god-mode-update-cursor ()
;;       (setq cursor-type (if (or god-local-mode buffer-read-only)
;;                             'box
;;                           'bar)))
;;     (add-hook 'god-mode-enabled-hook #'my-god-mode-update-cursor)
;;     (add-hook 'god-mode-disabled-hook #'my-god-mode-update-cursor)
;;     ;(define-key isearch-mode-map (kbd "<escape>") #'god-mode-isearch-activate)
;;     ;(define-key god-mode-isearch-map (kbd "<escape>") #'god-mode-isearch-disable)
;;     (add-to-list 'god-exempt-major-modes 'dired-mode)))

;; speed up lsp by disabling lsp-ui-mode
(after! lsp-mode
  (setq lsp-lens-enable nil))

(after! centaur-tabs
  (centaur-tabs-mode -1)
  (setq centaur-tabs-height 30
        centaur-tabs-set-icons t
        centaur-tabs-modified-marker "o"
        centaur-tabs-close-button "×"
        centaur-tabs-set-bar 'above
        centaur-tabs-gray-out-icons 'buffer)
  (centaur-tabs-change-fonts "Noto Sans Mono Regular" 80)
  )

;; from https://github.com/org-roam/org-roam/issues/931#issuecomment-666262675
(add-hook! org-roam-backlinks-mode
  (setq bidi-paragraph-direction nil
        bidi-paragraph-start-re "^"
        bidi-paragraph-separate-re "^")
  )
(add-hook! org-mode (org-roam-backlinks-mode))

(after! spell-fu-mode
  (ispell-change-dictionary "english" t))

;; (after! treemacs
;;   (defun treemacs-ignore-gitignore (file _)
;;     (string= file ".gitignore"))
;;   (push #'treemacs-ignore-gitignore treemacs-ignored-file-predicates))

(setq default-input-method "farsi-isiri-9147")
